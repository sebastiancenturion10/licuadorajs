var estado = "off";
var licuadora = document.getElementById("blender");
var sonidoLicuadora = document.getElementById("blender-sound");
var botonLicuadora = document.getElementById("blender-button-sound");

function onOff() {
  if (estado == "off") {
    estado = "on";
    sonar();
    licuadora.classList.add("active");
  } else {
    estado = "off";
    sonar();
    licuadora.classList.remove("active");
  }
}

function sonar() {
  if (sonidoLicuadora.paused) {
    botonLicuadora.play();
    sonidoLicuadora.play();
  } else {
    botonLicuadora.play();
    sonidoLicuadora.pause();
    sonidoLicuadora.currentTime = 0; /* Reinicia el audio al segundo 0 */
  }
}
